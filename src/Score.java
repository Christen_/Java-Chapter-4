/**
 * Created by pc on 3/17/16.
 */
public class Score {
    public static void main(String[] args) {
        int[] scores = {1, 2, 3, 4, 5, 6, 7};
        for (int score : scores) {
            System.out.printf("score: %d %n", score);
        }
    }
}
